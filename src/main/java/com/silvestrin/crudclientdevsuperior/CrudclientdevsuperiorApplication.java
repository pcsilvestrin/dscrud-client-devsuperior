package com.silvestrin.crudclientdevsuperior;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudclientdevsuperiorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudclientdevsuperiorApplication.class, args);
	}

}
