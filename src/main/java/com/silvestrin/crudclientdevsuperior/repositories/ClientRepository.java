package com.silvestrin.crudclientdevsuperior.repositories;

import com.silvestrin.crudclientdevsuperior.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

}
