package com.silvestrin.crudclientdevsuperior.resources.exceptions;

import com.silvestrin.crudclientdevsuperior.services.exceptions.DataBaseException;
import com.silvestrin.crudclientdevsuperior.services.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class) //Captura a exceção do tipo EntityNotFoundException
    public ResponseEntity<StandardError> entityNotFound(ResourceNotFoundException e, HttpServletRequest request){
        StandardError err = new StandardError();
        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.NOT_FOUND.value());
        err.setError("Resource not Found");
        err.setMessage(e.getMessage());       //Mensagem informada no Service
        err.setPath(request.getRequestURI()); //Pega o caminho onde foi feito a requisição Ex.:"/categories/5"

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
    }

    @ExceptionHandler(DataBaseException.class)
    public ResponseEntity<StandardError> database(DataBaseException e, HttpServletRequest request){
        StandardError err = new StandardError();
        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.BAD_REQUEST.value());
        err.setError("Database exception");
        err.setMessage(e.getMessage());       //Mensagem informada no Service
        err.setPath(request.getRequestURI()); //Pega o caminho onde foi feito a requisição Ex.:"/categories/5"

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

}
